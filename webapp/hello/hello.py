from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello OpenShift!'


def make_app():
    return app
